<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\pakaian;

class PakaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pakaian = file_get_contents(base_path('database/seeders/pakaian.json'));
        $pakaianJson = json_decode($pakaian);

        pakaian::truncate();
        foreach($pakaianJson as $pakaian) {
            pakaian::create((array) $pakaian);
        }
    }
}
